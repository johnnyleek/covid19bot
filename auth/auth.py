import csv

def get_tokens():
    print("Retrieving Access Tokens")
    tokens = {}
    with open('auth/ACCESS_TOKENS', mode='r') as tokens_file:
        file_reader = csv.DictReader(tokens_file)
        tokens = next(file_reader)
    return tokens
