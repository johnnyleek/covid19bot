tweets = (
    ["SAFETY TIP\n\nLimiting contact with individuals is a key factor in the spread of an pandemic.\nThe less people we come into contact with, the slower a virus can spread. To see a simulation of these effects, check out this link: https://bit.ly/CoronaSimulator\n #coronavirus #SocialDistancing", None],
    ["SAFETY TIP\n\nIf you must travel, ensure that you bring hand sanitizer and disinfectant wipes and wipe down any surfaces in public before you touch them.\nThis will greatly decrease your risk of contracting the virus! #covid19, #coronavirus", None],
    ["SAFETY TIP\n\nHand Washing is crucial to stay healthy!\n\nMake sure to wash your hands with warm water for at least 20 seconds!\n\nSee the attached image for details!\n#coronavirus #covid19 #handwashing", 'handwashing.gif'],
    ["SAFETY TIP\n\nIf you think you may have Coronavirus, do NOT immediately go to the Emergency Room!\nFirst, call ahead and let the doctor's office know that you may be infected. They will give you further instruction! Keep the community safe!\n#coronavirus #covid19 #sick", None],
    ["COVID FACT\n\nThe highest risk demographic are those over 80 years of age and ones with preexisting respiratory conditions.\nThe mortality rate ranges from 0.2% in young children to as high as 21.9% in those over 80.\n#coronavirus #covid19", None],
    ["SAFETY TIP\n\nThe incubation period for coronavirus is 2-14 days, but some have reported it being as long as 27 days.\nTake this into account before going out in public.\n\nYou may have it and have no idea!\n#coronavirus #covid19 #covidsafety", None],
    ["SAFETY TIP\n\nRemember to do the 5:\n1: HANDS - Wash them often\n2: ELBOW - Cough into it\n3. FACE - Don't touch it\n4. SPACE - Keep safe distance\n5. HOME - Stay if you can\n\n#coronavirus #covid19", None],
    ["SAFETY TIP\n\nThere are conflicting reports as to how long the coronavirus lasts on surfaces.\nThere are estimates ranging from a few hours to several days.\n\nBring disinfecting products with you to wipe down public services before use.\n#coronavirus #covid19", None],
    ["COVID FACT\n\nYou do not need to wear a facemask unless you are caring for someone who is sick.\nFacemasks are in short supply and should be reserved for caregivers.\n\nSource: cdc.gov\n#coronavirus #covid19", None],
    ["SAFETY TIP\n\nIf you experience:\nDifficulty breathing\nPressure in the chest area\nUnexplained confusion\nor bluish lips or face\nSeek medical attention immediately. These are COVID-19's emergency warning signs.\nSource: CDC.gov\n#coronavirus #covid19", None],

)
