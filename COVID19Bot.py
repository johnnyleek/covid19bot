import schedule
import time
import io
import pandas
import random
import requests
import csv

from datetime import date, timedelta
from twython import Twython as TwitterAPI

from auth import auth
import settings
import safety_tweets

class TwitterBot():
    def __init__(self):
        self.tokens = auth.get_tokens()
        self.twitterAPI = TwitterAPI(
            self.tokens['API'],
            self.tokens['API_SECRET'],
            self.tokens['ACCESS'],
            self.tokens['ACCESS_SECRET']
        )
        self.safety_tweet_idx = 2
        #Run all commands initially
        self.get_data()
        self.get_totals()
        self.sum_countries_with_provinces()
        print("Waiting until scheduled tweet time...")

        #Set scheduling
        schedule.every().day.at("10:00").do(self.tweet, "SAFETY")
        schedule.every().day.at("12:00").do(self.tweet, "SAFETY")
        schedule.every().day.at("14:00").do(self.tweet, "SAFETY")
        schedule.every().day.at("16:00").do(self.tweet, "SAFETY")
        schedule.every().day.at("19:00").do(self.write_data)
        schedule.every().day.at("20:14").do(self.get_data)
        schedule.every().day.at("20:15").do(self.get_totals)
        schedule.every().day.at("20:15").do(self.sum_countries_with_provinces)
        schedule.every().day.at("20:15").do(self.tweet, "TOTAL")
        schedule.every().day.at("20:30").do(self.tweet, "DEATHS")
        schedule.every().day.at("20:45").do(self.tweet, "STATS_GENERAL")
        schedule.every().day.at("21:00").do(self.tweet, "SAFETY")

        self.run_automated_tasks()

    #Downloads the most recent data from John Hopkins University
    def get_data(self):
        today_date = date.today()
        request = None
        while True:
            today_date_str = today_date.strftime("%m-%d-%Y")
            print("Trying to download data from %s" % today_date_str)
            data_url = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/{}.csv".format(today_date_str)
            request = requests.get(data_url)
            #If there was data for today, use that data
            if request != None and request.status_code != 404:
                break
            else:
                print("Couldn't find data for %s. Trying to find previous days data" % today_date_str)
                today_date = today_date - timedelta(days=1)
        self.data = pandas.read_csv(io.StringIO(request.content.decode('utf-8')))

    def get_total_confirmed(self):
        return self.data['Confirmed'].sum()

    def get_total_deaths(self):
        return self.data['Deaths'].sum()

    def get_total_recovered(self):
        return self.data['Recovered'].sum()

    def get_total_countries_infected(self):
        return len(self.data.index)

    def get_totals(self):
        self.total_confirmed = self.get_total_confirmed()
        self.total_deaths = self.get_total_deaths()
        self.total_recovered = self.get_total_recovered()
        self.total_countries_infected = self.get_total_countries_infected()

    def sum_countries_with_provinces(self):
        self.data = self.data.groupby('Country_Region').sum()

    def get_sorted_data(self, column, amt, ascending_order):
        sortedFrame = self.data.sort_values(by=[column], ascending=ascending_order)[:amt]
        return sortedFrame

    def get_change(self, country, data_type):
        prev_data = pandas.read_csv('data/previous_data.csv')
        amt = prev_data.loc[prev_data['Country_Region'] == country, data_type].sum()
        if country == 'ALL':
            if data_type == 'Confirmed':
                change = int(round(self.total_confirmed - amt))
            elif data_type == 'Deaths':
                change = int(round(self.total_deaths - amt))
            elif data_type == 'Recovered':
                change = int(round(self.total_recovered - amt))
        else:
            change = abs(int(round(self.data.loc[country, data_type].sum() - amt)))

        return "+{}".format(change)

    def total_tweet(self):
        message = ""
        message += "COVID-19 UPDATE {}\n\n".format(date.today().strftime("%m-%d-%Y"))
        message += "Total Confirmed Cases: {}\n".format(self.total_confirmed, self.get_change('ALL', 'Confirmed'))
        message += "Total Deaths: {}\n".format(self.total_deaths, self.get_change('ALL', 'Deaths'))
        message += "Total Recovered: {}\n\n".format(self.total_recovered, self.get_change('ALL', 'Recovered'))
        message += "TOP 3 COUNTRIES:\n"
        for index, row in self.get_sorted_data('Confirmed', 3, False).iterrows():
            message += settings.flags[index] + " " + str(index) + ": " + str(int(row['Confirmed'])) + " Cases, " + str(int(row['Deaths'])) + " Deaths, " + str(int(row['Recovered'])) + " Recovered\n"
        return [message, None]

    def increase_tweet(self):
        pass

    def deaths_tweet(self):
        message = ""
        message += "COVID-19 DEATH UPDATE {}\n\n".format(date.today().strftime("%m-%d-%Y"))
        message += "TOP 3 COUNTRIES (By Deaths):\n"
        for index, row in self.get_sorted_data('Deaths', 3, False).iterrows():
            message += "{} {}: {} Cases, {} Deaths ({}), {} Recovered\n".format(settings.flags[index], index, int(round(row['Confirmed'])),
                                                                                                                      int(round(row['Deaths'])), self.get_change(index, 'Deaths'),
                                                                                                                      int(round(row['Recovered'])))
        return [message, None]

    def safety_tweet(self):
        if self.safety_tweet_idx > len(safety_tweets.tweets) - 1:
            self.safety_tweet_idx = 0
        selection = safety_tweets.tweets[self.safety_tweet_idx]
        media_img = None
        message = selection[0]
        if selection[1]:
            media_img = "img/" + selection[1]
        self.safety_tweet_idx += 1
        return [message, media_img]

    def general_stat_tweet(self):
        message = ""
        message += "COVID-19 General Stats\n\n"
        message += "As of today ({}), there are {} different countries/areas that have reported the disease.\n\n".format(date.today().strftime("%m-%d-%Y"), self.get_total_countries_infected())
        message += "Today there were:\n\n"
        message += "{} more confirmed cases,\n".format(self.get_change('ALL', 'Confirmed'))
        message += "{} more deaths,\n".format(self.get_change('ALL', 'Deaths'))
        message += "and {} more recoveries.\n\n".format(self.get_change('ALL', 'Recovered'))
        message += "#covid19 #coronavirus"
        return [message, None]

    def tweet(self, tweet_type):
        tweet_msg = ""
        if tweet_type == "TOTAL":
            tweet_msg = self.total_tweet()
        elif tweet_type == "STATS_GENERAL":
            tweet_msg = self.general_stat_tweet()
        elif tweet_type == "DEATHS":
            tweet_msg = self.deaths_tweet()
        elif tweet_type == "SAFETY":
            tweet_msg = self.safety_tweet()

        print("Constructed Tweet:\n%s" % tweet_msg[0])
        if tweet_msg[1]:
            print("Image: %s" % tweet_msg[1])
        if(settings.settings["ACTIVE"]):
            print("Sending Tweet...")
            if tweet_msg[1]:
                img = open(tweet_msg[1], "rb")
                response = self.twitterAPI.upload_media(media=img)
                media_id = [response['media_id']]
                self.twitterAPI.update_status(status=tweet_msg[0],media_ids=media_id)
            else:
                self.twitterAPI.update_status(status=tweet_msg[0])
            print("Tweet Sent!")
        print("Waiting until scheduled tweet time...")

    def run_automated_tasks(self):
        while True:
            schedule.run_pending()
            time.sleep(1)

    def write_data(self):
        with open('data/previous_data.csv', 'w', newline='') as csvfile:
            headers = ['Country_Region', 'Confirmed', 'Deaths', 'Recovered']
            writer = csv.DictWriter(csvfile, fieldnames=headers)
            writer.writeheader()
            writer.writerow({'Country_Region': "ALL", 'Confirmed': self.total_confirmed, 'Deaths': self.total_deaths, 'Recovered': self.total_recovered})
            for index, row in self.data.iterrows():
                writer.writerow({'Country_Region': index, 'Confirmed': row['Confirmed'], 'Deaths': row['Deaths'], 'Recovered': row['Recovered']})

twitter = TwitterBot()
